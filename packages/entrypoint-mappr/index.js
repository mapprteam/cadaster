const typeDefs = require('@cadaster/api-graphql/src/schema/typeDefs')
const resolvers = require('@cadaster/api-graphql/src/schema/resolvers')

module.exports = {
  typeDefs,
  resolvers
}
